var csv = require('csv-parser');
var fs = require('fs');
const { v4: uuidv4 } = require('uuid');
require('dotenv').config()
var filename = process.env.FILENAME || "titanic.csv";
var out_filename=filename.split(".")[0]
var dataArray = [];

fs.createReadStream(filename)
  .pipe(csv())
  .on('data', function (data) {
    data.uuid = uuidv4();
    dataArray.push(data);
  })
  .on('end', function(){
    fs.writeFileSync(`${out_filename}.json`, JSON.stringify(dataArray));
  });
  console.log("CSV UUID ADD : Script Finished")