# Node.js Express & MongoDB: CRUD Rest API for Titanic
## General prerequisites

Running with Docker Compose

[Docker](https://docs.docker.com/get-docker/)

[Docker-compose]( https://docs.docker.com/compose/install/)

Running inside Kubernetes minikube cluster

[kubectl](https://kubernetes.io/docs/tasks/tools/)

[minikube](https://minikube.sigs.k8s.io/docs/start/)

## Running with Docker Compose

1) Use pre-populated example `.env` or adjust with your own settings . After this is done, please add it to `.gitignore` so it is not saved

2) Bootstap compose stack
```
docker-compose up
```
3) Visit `127.0.0.1:8080`

4) You should see `"{"message":"Welcome to Titanic application."}"`

Cleaning up : 
```
docker-compose down -v
```
## Running inside Kubernetes minikube cluster

1) Start minikube: 
```
minikube start
```
2) Export minikube variables, so we can build and use local images
```
eval $(minikube docker-env)
```
3) Build the images via compose with exported vars pointing to minikube docker daemon
```
docker-compose build
```
4)  Use pre-populated example  `.env-k8sfile` or adjust with your own settings . After this is done, please add it to `.gitignore` so it is not saved in git history. 

Please be aware that deployments & services doesnt support variables for ports values , so inside of Kubernetes cluster, please don't change ports in `.env-k8sfile` file. For this to happen we would need to use something like helm or kustomize

5) Creating Kubernetes Secret : 
```
kubectl create secret generic dev-secret --from-env-file=.env-k8s
```
6) Apply all manifests from the k8s directory :
```
kubectl apply -f k8s --recursive
```
7) Expose service : 
```
kubectl port-forward service/mongo-express 8080:8080 
```
8) Visit the service :

Visit `127.0.0.1:8080`


Cleaning up : 
```
kubectl delete -f k8s --recursive
kubectl delete secret dev-secret
minikube stop
```
Close terminal,or refresh environment variables if you want to use docker-compose stack.
As variables will prevent opening app on `127.0.0.1`

## How to use .env

`MONGODB_USERNAME` - Admin & app username with which Mongo DB will boot

`MONGODB_PASSWORD` - Admin & app username

`MONGODB_DATABASE` - Defaut DB defined on Mongo Start & one used in Backend NodeJS app

`MONGODB_PORT_NUMBER` - Mongo DB port. 

`DB_HOST` - Hostname used when connecting to DB. In Kubernetes cluster equal to a service name

`PORT` - PORT used in backend .

`FILENAME` - filename used in csv to json convert script. 


## Flow
1) NodeJS connects to MONGO DB
2) Mongoimport container or kubernetes job converts csv data during build to a JSON, import JSON into MongoDB collection during runtime

## Possible imporvments 

- Template the manifest via HELM
- Launch in managed K8S cluster with ingress controller like Traefik or Nginx , so will be no need to use dev approach with port forwarding
- use sealed secrets or external credential store like Vault
- Move DB to a managed service