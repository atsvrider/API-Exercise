
require('dotenv').config()
var username = process.env.MONGODB_USERNAME
var password = process.env.MONGODB_PASSWORD
var database = process.env.MONGODB_DATABASE
var db_port = process.env.MONGODB_PORT_NUMBER
var host = process.env.DB_HOST
console.log(`mongodb://${username}:${password}@${host}:${db_port}/${database}`)

module.exports = {
  url: `mongodb://${username}:${password}@${host}:${db_port}/${database}`
};
