module.exports = mongoose => {
    var schema = mongoose.Schema(
      {

        survived : Boolean,
        passengerClass: Number, 
        name : String,
        sex: String,
        siblingsOrSpousesAboard:	Number,
        parentsOrChildrenAboard: 	Number,
        fare	: 	Number,
        uuid : String
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();

      return object;
    });
  
    const People = mongoose.model("people", schema);
    return People;
  };
  