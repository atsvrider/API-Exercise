module.exports = mongoose => {
    var schema = mongoose.Schema(
      {

        survived : Boolean,
        passengerClass: Number, 
        name : String,
        sex: String,
        siblingsOrSpousesAboard:	Number,
        parentsOrChildrenAboard: 	Number,
        fare	: 	Number
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
    });
  
    const personData = mongoose.model("personData", schema);
    return personData;
  };
  