const { v4: uuidv4 } = require('uuid');
const db = require("../models");

const People = db.people;



// Create and Save a new People
exports.create = (req, res) => {
  // Validate request
  if (!req.body.survived) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  // Create a new people
  const people = new People({

    survived : req.body.survived,
    passengerClass: req.body.passengerClass, 
    name : req.body.name,
    sex: req.body.sex,
    siblingsOrSpousesAboard:	req.body.siblingsOrSpousesAboard,
    parentsOrChildrenAboard: 	req.body.parentsOrChildrenAboard,
    fare	: 	req.body.fare,
    uuid : uuidv4()

  });

  // Save people in the database
  people
    .save(people)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the people."
      });
    });
};

// Retrieve all people from the database.
exports.findAll = (req, res) => {

  var condition = {};

  People.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving people."
      });
    });
};

// Find a single people with an uuid
exports.findOne = (req, res) => {
  const uuid = req.params.uuid;
  var condition = {"uuid":uuid};
  console.log("condition: ",condition,req.params)
  People.find(condition)
    .then(data => {
      if (!data)
        res.status(404).send({ message: "Not found People with uuid " + uuid });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving People with uuid=" + uuid });
    });
};

// Update a People by the uuid in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const uuid = req.params.uuid;
  var condition = {"uuid":uuid};
  People.findOneAndUpdate(condition, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update People with uuid=${uuid}. Maybe People was not found!`
        });
      } else res.send({ message: "People was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating People with uuid=" + uuid
      });
    });
};

// Delete a People with the specified uuid in the request
exports.delete = (req, res) => {
  const uuid = req.params.uuid;
  var condition = {"uuid":uuid};
  People.findOneAndDelete(condition, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete People with uuid=${uuid}. Maybe People was not found!`
        });
      } else {
        res.send({
          message: "People was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete People with uuid=" + uuid
      });
    });
};
