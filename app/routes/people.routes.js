module.exports = app => {
  // const tutorials = require("../controllers/tutorial.controller.js");
  const people = require("../controllers/people.controller.js");
  var router = require("express").Router();

  // Get a list of all people
  router.get("/", people.findAll);
  // Add a person to the database
  router.post("/", people.create);

  // Get information about one person
  router.get("/:uuid", people.findOne);

  // Update information about one person
  router.put("/:uuid", people.update);

  // Delete this person
  router.delete("/:uuid", people.delete);


  app.use("/people", router);
};
